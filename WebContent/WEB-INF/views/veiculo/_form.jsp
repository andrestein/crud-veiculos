<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="form-group">
	<label for="fabricante">Fabricante</label> 
	<input type="text" class="form-control" name="fabricante" placeholder="GM, Ford, ..." value="${veiculo.fabricante}"/>
	<span class="form-errors"><form:errors path="veiculo.fabricante" /></span>
</div>
<div class="form-group">
	<label for="modelo">Modelo</label> 
	<input type="text" class="form-control" name="modelo" placeholder="Gol, Sandelo, Chevette, ..." value="${veiculo.modelo}"/>
	<span class="form-errors"><form:errors path="veiculo.modelo" /></span>
</div>
<div class="form-group">
	<label for="Ano">Ano</label> 
	<input type="text" class="form-control" name="ano" placeholder="2010, 2011, ..." value="${veiculo.ano}"/>
	<span class="form-errors"><form:errors path="veiculo.ano" /></span>
</div>
<div class="form-group">
	<label for="foto">Foto</label> 
	<input type="file" name="file">
	<p class="help-block">Formatos aceitos: JPG e PNG </p>
	<span class="form-errors"><form:errors path="veiculo.file" /></span>
	<c:if test="${veiculo.foto != '' && veiculo.foto != null}">
		<div>
			<p><img src='<c:url value="/images/${veiculo.foto}" />'/></p>
			<a href='<c:url value="/deleteImage?id=${veiculo.id}"/>' class="btn btn-danger btn-sm btn-delete-image">
				<i class="glyphicon glyphicon-remove"></i> Excluir imagem
			</a>
		</div>
	</c:if>
</div>