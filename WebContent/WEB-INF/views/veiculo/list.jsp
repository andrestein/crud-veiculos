<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Crud Web</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/bootstrap-3.3.6/css/bootstrap.min.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/main.css' />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

<body>
    <div class="container">
		<%@ include file="../layout/_navbar.jsp" %>

		<ol class="breadcrumb">
		  	<li><a href="<c:url value='/' />">Home</a></li>
		  	<li><a href="<c:url value='/list' />">Veiculos</a></li>
		  	<li class="active">Listagem de veículos</li>
		  	<li class="pull-right btn-breadcrumb">
		  		<a href="<c:url value='/create' />" class="btn btn-default btn-xs"> <i class="glyphicon glyphicon-plus"></i> Novo Veículo</a>
		  	</li>
		</ol>

		<c:if test="${param.message != '' && param.message != null }">
			<div class="alert alert-success">
				${param.message}
			</div>
		</c:if>

		<table class="table table-striped table-hover table-condensed">
  			<thead>
  				<tr>
  					<th>Código</th>
  					<th>Foto</th>
  					<th>Fabricante</th>
  					<th>Modelo</th>
  					<th>Ano</th>
  					<th class="acao"></th>
  				</tr>
  			</thead>
  			<tbody>
  				<c:if test="${veiculos.size() > 0}">
	  				<c:forEach items="${veiculos}" var="item">
		  				<tr>
		  					<td>${item.id}</td>
		  					<td>
		  						<c:if test="${item.foto != '' && item.foto != null}">
			  						<img src='<c:url value="/images/${item.foto}" />'/>
		  						</c:if>
		  					</td>
		  					<td>${item.fabricante}</td>
		  					<td>${item.modelo}</td>
		  					<td>${item.ano}</td>
		  					<td>
		  						<a href="<c:url value='/edit/?id=${item.id}' />" class="btn btn-default btn-xs">
		  							<i class="glyphicon glyphicon-edit"></i>
		  						</a>
		  						<a href="<c:url value='/delete/?id=${item.id}' />" class="btn btn-danger btn-xs btn-delete">
		  							<i class="glyphicon glyphicon-trash"></i>
		  						</a>
		  					</td>
		  				</tr>
	  				</c:forEach>
  				</c:if>
  				<c:if test="${!(veiculos.size() > 0)}">
  					<tr>
  						<td colspan="6" class="not-found">Nenhum veículo encontrado</td>
  					</tr>
  				</c:if>
  			</tbody>
		</table>

    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/main.js' />"></script>
    <script src="<c:url value='/resources/bootstrap-3.3.6/js/bootstrap.min.js' />"></script>
</body>
</html>
