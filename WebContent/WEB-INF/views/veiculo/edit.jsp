<%@page import="org.springframework.context.annotation.ImportResource"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Crud Web</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/bootstrap-3.3.6/css/bootstrap.min.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/main.css' />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

<body>
    <div class="container">
		<%@ include file="../layout/_navbar.jsp" %>

		<ol class="breadcrumb">
		  	<li><a href="<c:url value='/' />">Home</a></li>
		  	<li><a href="<c:url value='/list' />">Veiculos</a></li>
		  	<li class="active">Atualizar veículo</li>
		  	<li class="pull-right btn-breadcrumb">
		  		<a href="<c:url value='/list' />" class="btn btn-default btn-xs"> <i class="glyphicon glyphicon-list"></i> Lista Veículos</a>
		  	</li>
		  	<li class="pull-right btn-breadcrumb">
		  		<a href="<c:url value='/create' />" class="btn btn-default btn-xs"> <i class="glyphicon glyphicon-plus"></i> Novo Veículo</a>
		  	</li>
		</ol>

		<c:if test="${param.message != '' && param.message != null }">
			<div class="alert alert-success">
				${param.message}
			</div>
		</c:if>

		<form action='<c:url value="/update"/>' method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" value="${veiculo.id}"/>
			<input type="hidden" name="foto" value="${veiculo.foto}"/>
			<%@ include file="_form.jsp"%>
			<div class="buttons">
				<button type="submit" class="btn btn-primary btn-lg">
					<i class="glyphicon glyphicon-floppy-saved"></i> ATUALIZAR
				</button>
				<a href='<c:url value="/delete?id=${veiculo.id}"/>' class="btn btn-danger btn-lg btn-delete">
					<i class="glyphicon glyphicon-remove"></i> EXCLUIR
				</a>
			</div>
		</form>

	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/main.js' />"></script>
    <script src="<c:url value='/resources/bootstrap-3.3.6/js/bootstrap.min.js' />"></script>
</body>
</html>
