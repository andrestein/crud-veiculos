;(function(window, document,undefined) {
    
    'use strict';
    var main = (function(){
        var exports = {};
        
        var _confirmDelete = function() {
        	$(".btn-delete").click(function() {
        		return confirm("Confirma a exclusão do registro?");
        	});
        };
        
        var _confirmDeleteImage = function() {
        	$(".btn-delete-image").click(function() {
        		return confirm("Confirma a exclusão da imagem?");
        	});
        };
        
        var _init = function(){
        	_confirmDelete();
        	_confirmDeleteImage();
        };
        
        exports.init = _init;
        return exports;
        
    })();
    
    window.main = main;
    main.init();
    
})(window, document);