package br.com.arezzo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arezzo.model.Usuario;

/**
 * @author André Stein
 * 
 */
@Repository
public class UsuarioDAO {
	
	private Connection connection;

	@Autowired
	public UsuarioDAO(DataSource ds) {
		try {
			connection = ds.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Responsável pela autenticação do usuário.
	 * 
	 * @param usuario
	 * @return
	 */
	public boolean authentication(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário nao deve ser nulo");
		}
		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("select * from usuarios where login = ? and senha = ?");
			stmt.setString(1, usuario.getLogin());
			stmt.setString(2, usuario.getSenha());
			ResultSet rs = stmt.executeQuery();

			boolean exists = rs.next();
			rs.close();
			stmt.close();

			return exists;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param usuario
	 */
	public void save(Usuario usuario) {
		if (usuario == null) {
			throw new IllegalArgumentException("Usuário nao deve ser nulo");
		}

		try {
			PreparedStatement stmt = this.connection
					.prepareStatement("insert into usuarios (login,senha) values (?,?)");
			stmt.setString(1, usuario.getLogin());
			stmt.setString(2, usuario.getSenha());
			stmt.execute();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
