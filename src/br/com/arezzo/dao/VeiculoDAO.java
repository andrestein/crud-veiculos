package br.com.arezzo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.arezzo.model.Veiculo;

/**
 * @author André Stein
 * 
 */
@Repository
public class VeiculoDAO {

	private Connection connection;

	@Autowired
	public VeiculoDAO(DataSource ds) {
		try {
			this.connection = ds.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Insere veículo
	 * 
	 * @param veiculo
	 */
	public void save(Veiculo veiculo) {
		String sql = "insert into veiculos (fabricante, modelo, ano, foto) values (?,?,?,?)";
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, veiculo.getFabricante());
			stmt.setString(2, veiculo.getModelo());
			stmt.setInt(3, veiculo.getAno());
			stmt.setString(4, veiculo.getFoto());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Deletar veiculo
	 * 
	 * @param veiculo
	 */
	public void delete(Veiculo veiculo) {
		if (veiculo.getId() == null) {
			throw new IllegalStateException("Id da conta não deve ser nula.");
		}

		String sql = "delete from veiculos where veiculo_id = ?";
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, veiculo.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Atualizar veículo
	 * @param veiculo
	 */
	public void update(Veiculo veiculo) {
		String sql = "update veiculos set fabricante = ?, modelo = ?, ano = ?, foto = ? where veiculo_id = ?";
		PreparedStatement stmt;
		try {
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, veiculo.getFabricante());
			stmt.setString(2, veiculo.getModelo());
			stmt.setInt(3, veiculo.getAno());
			stmt.setString(4, veiculo.getFoto());
			stmt.setInt(5, veiculo.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Listar veículos
	 * @return
	 */
	public List<Veiculo> list() {
		try {
			List<Veiculo> veiculos = new ArrayList<Veiculo>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from veiculos");
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {	// adiciona a conta na lista
				veiculos.add(populaVeiculo(rs));
			}
			rs.close();
			stmt.close();
			return veiculos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	/**
	 * Busca o veículo por id
	 * @param id
	 * @return
	 */
	public Veiculo get(Integer id) {
		if (id == null) {
			throw new IllegalStateException("Id do veículo nao deve ser nulo.");
		}

		try {
			PreparedStatement stmt = this.connection.prepareStatement("select * from veiculos where veiculo_id = ?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				return populaVeiculo(rs);
			}
			rs.close();
			stmt.close();
			return null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Popula o objeto Veiculo
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Veiculo populaVeiculo(ResultSet rs) throws SQLException {
		Veiculo veiculo = new Veiculo();
		veiculo.setId(rs.getInt("veiculo_id"));
		veiculo.setFabricante(rs.getString("fabricante"));
		veiculo.setModelo(rs.getString("modelo"));
		veiculo.setAno(rs.getInt("ano"));
		veiculo.setFoto(rs.getString("foto"));
		return veiculo;
	}

}
