package br.com.arezzo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.arezzo.dao.UsuarioDAO;
import br.com.arezzo.model.Usuario;

/**
 * @author André Stein
 * 
 */
@Controller
public class LoginController {

	private UsuarioDAO dao;

	@Autowired
	public LoginController(UsuarioDAO dao) {
		this.dao = dao;
	}

	/**
	 * Carrega a página de login
	 * 
	 * @return
	 */
	@RequestMapping("/login")
	public String login(HttpServletRequest request) {
		if (request.getSession().getAttribute("usuarioLogado") != null) {
			return "redirect:/list";
		}
		return "/login/index";
	}

	/**
	 * Faz a autenticação do usuário
	 * 
	 * @param usuario
	 * @return
	 */
	@RequestMapping("/authenticate")
	public String authenticate(Usuario usuario, HttpSession session) {
		if (dao.authentication(usuario)) {
			session.setAttribute("usuarioLogado", usuario);
			return "redirect:/list";
		}
		return "redirect:/login";
	}

	/**
	 * Faz o logout
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}

}
