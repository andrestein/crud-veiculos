package br.com.arezzo.controller;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.arezzo.dao.VeiculoDAO;
import br.com.arezzo.model.Veiculo;

/**
 * @author André Stein
 * 
 */
@Controller
public class VeiculoController {

	private VeiculoDAO dao;

	@Autowired
	public VeiculoController(VeiculoDAO dao) {
		this.dao = dao;
	}

	/**
	 * home
	 * 
	 * @return
	 */
	@RequestMapping("/")
	public String home() {
		return "forward:list";
	}

	/**
	 * Lista todos os veículos
	 * 
	 * @return
	 */
	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView("/veiculo/list");
		mv.addObject("veiculos", dao.list());
		return mv;
	}

	/**
	 * Carrega o formulário de cadastro de veículos.
	 * 
	 * @return
	 */
	@RequestMapping("/create")
	public ModelAndView create() {
		ModelAndView mv = new ModelAndView("/veiculo/create");
		mv.addObject("veiculos", new Veiculo());
		return mv;
	}

	/**
	 * Inserir veículo
	 * 
	 * @param veiculo
	 * @return
	 */
	@RequestMapping("/save")
	 public ModelAndView save(@Valid Veiculo veiculo, BindingResult result) {
		 ModelAndView mv = null;
		 if (result.hasErrors()) {
			 mv = new ModelAndView("/veiculo/create");
			 mv.addObject("veiculo", veiculo);
			 return mv;
		 }
		
		 imageVeiculo(veiculo);
		 dao.save(veiculo);
		
		 mv = new ModelAndView("redirect:/list");
		 mv.addObject("message", "Veículo cadastrado com sucesso");
		 return mv;
	 }

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping("/edit/**")
	public ModelAndView edit(Integer id) {
		Veiculo veiculo = dao.get(id);
		ModelAndView mv = null;
		if (veiculo != null) {
			mv = new ModelAndView("/veiculo/edit");
			mv.addObject("veiculo", veiculo);
		} else {
			mv = new ModelAndView("/error/404");
		}
		return mv;
	}

	/**
	 * Inserir veículo
	 * 
	 * @param veiculo
	 * @return
	 */
	@RequestMapping("/update")
	public ModelAndView update(@Valid Veiculo veiculo, BindingResult result) {
		ModelAndView mv = null;
		if (result.hasErrors()) {
			mv = new ModelAndView("/veiculo/edit");
			mv.addObject("veiculo", veiculo);
			return mv;
		}
		imageVeiculo(veiculo);
		dao.update(veiculo);
	
		mv = new ModelAndView("redirect:/list");
		mv.addObject("message", "Veículo atualizado com sucesso");
		return mv;
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete/**")
	public ModelAndView delete(Integer id) {
		ModelAndView mv = new ModelAndView("redirect:/list");
		try {
			dao.delete(dao.get(id));
			mv.addObject("message", "Veículo excluído com sucesso.");
		} catch (Exception e) {
			mv.addObject("message", e.getMessage());
		}
		return mv;
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteImage/**")
	public ModelAndView deleteImage(Integer id) {
		ModelAndView mv = new ModelAndView("redirect:/edit?id=" + id);
		try {
			Veiculo veiculo = dao.get(id);
			File foto = new File("/img_veiculos/" + veiculo.getFoto());
			foto.delete();
			veiculo.setFoto("");
			dao.update(veiculo);
			mv.addObject("message", "Imagem excluída com sucesso");
		} catch (Exception e) {
			mv.addObject("message", e.getMessage());
		}
		return mv;
	}
	
	
	
	/**
	 * @param veiculo
	 */
	private void imageVeiculo(Veiculo veiculo) {
		MultipartFile file = veiculo.getFile();
		if (file != null && !file.isEmpty()) {
			try {
				byte[] bytes = scale(file.getBytes(), 200, 200);
				File dir = new File("/img_veiculos");
				File serverFile = new File(dir.getAbsolutePath() + "\\"
						+ file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				veiculo.setFoto(file.getOriginalFilename());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param fileData
	 * @param width
	 * @param height
	 * @return
	 */
	private byte[] scale(byte[] fileData, int width, int height) {
		ByteArrayInputStream in = new ByteArrayInputStream(fileData);
		try {
			BufferedImage img = ImageIO.read(in);
			if (height == 0) {
				height = (width * img.getHeight()) / img.getWidth();
			}
			if (width == 0) {
				width = (height * img.getWidth()) / img.getHeight();
			}
			Image scaledImage = img.getScaledInstance(width, height,
					Image.SCALE_SMOOTH);
			BufferedImage imageBuff = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_RGB);
			imageBuff.getGraphics().drawImage(scaledImage, 0, 0,
					new Color(0, 0, 0), null);

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			ImageIO.write(imageBuff, "jpg", buffer);

			return buffer.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
