package br.com.arezzo.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.arezzo.TestContext;
import br.com.arezzo.controller.VeiculoController;
import br.com.arezzo.dao.VeiculoDAO;
import br.com.arezzo.model.Veiculo;

/**
 * @author André Stein
 * 
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = { TestContext.class })
@WebAppConfiguration
public class VeiculoControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private VeiculoController veiculoController;

	@Mock
	private VeiculoDAO dao;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(veiculoController).build();
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void editNotFoundTest() throws Exception {
		Veiculo veiculo = new Veiculo();
		Mockito.when(dao.get(1)).thenReturn(veiculo);

		mockMvc.perform(get("/edit/1")) 
				.andExpect(status().isOk())
				.andExpect(view().name("/error/404"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void editTest() throws Exception {
		Veiculo veiculo = new Veiculo();
		Mockito.when(dao.get(1)).thenReturn(veiculo);

		mockMvc.perform(get("/edit").param("id", "1")) 
		   .andExpect(status().isOk())
		   .andExpect(view().name("/veiculo/edit"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void createTest() throws Exception {
		mockMvc.perform(get("/create"))
			.andExpect(status().isOk())
			.andExpect(view().name("/veiculo/create"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void listTest() throws Exception{
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		Veiculo veiculo1 = new Veiculo();
		Veiculo veiculo2 = new Veiculo();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		
		Mockito.when(dao.list()).thenReturn(veiculos);
		
		mockMvc.perform(get("/list"))
			.andExpect(status().isOk())
			.andExpect(view().name("/veiculo/list"))
			.andExpect(model().attribute("veiculos", veiculos));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void deleteTest() throws Exception  {
		Veiculo veiculo = new Veiculo();
		Mockito.doNothing().when(dao).delete(veiculo);
		Mockito.when(dao.get(1)).thenReturn(veiculo);
		
		mockMvc.perform(get("/delete/1"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/list"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void saveTest() throws Exception{
		Veiculo veiculo = new Veiculo();
		Mockito.doNothing().when(dao).save(veiculo);
		
		mockMvc.perform(post("/save")
				.param("modelo", "m")
				.param("ano", "1111")
				.param("fabricante", "dfd"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/list"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void saveFailTest() throws Exception{
		Veiculo veiculo = new Veiculo();
		Mockito.doNothing().when(dao).save(veiculo);
		
		mockMvc.perform(post("/save")
				.param("modelo", "")
				.param("ano", "1111")
				.param("fabricante", "dfd"))
			.andExpect(status().isOk())
			.andExpect(view().name("/veiculo/create"));
	}
	
	/**
	 * @throws Exception
	 */
	@Test
	public void updateTest() throws Exception{
		Veiculo veiculo = new Veiculo();
		Mockito.doNothing().when(dao).save(veiculo);
		
		mockMvc.perform(post("/save")
				.param("modelo", "m")
				.param("ano", "1111")
				.param("fabricante", "dfd"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/list"));
	}

}
